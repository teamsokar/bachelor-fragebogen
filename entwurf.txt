Unternehmensprofil
	- Gr��e
	- Branche
	- Arbeitszeitmodell (Gleitzeit, Tagarbeit (Normalschicht), 2-Schicht, Mehrschicht)
	
Aufbau einer Messstation (unter Annahme, dass es nur eine ist bzw. ein Typus)
	- Software
		- Welches Betriebssystem verwenden Sie? (XP, Vista, Win7)
		- Welche Software von IMT ist im Einsatz? (Scanware, U-Soft, MyCalypso, andere)
		- Welche weitere Software verwenden Sie bei Messaufgaben (Freitext)
		
	- Hardware
		- welche Hardware-Ausstattung verwendenn sie? (Desktop-PC, Workstation, andere)
		- welche Messmaschinen sind im Einsatz (ScanMax, DuraMax, andere)
		
Allgemein
	- Bewerten sie Ihre Zufriedenheit mit den Aspekten der eingesetzten Software (1-5)
		- Bedienbarkeit
		- Geschwindigkeit / Messleistung
		- Funktionsumfang
		- ...
		
	- Nutzen sie M�glichkeiten zur Automatisierug wie Makros? (JN)
	- Haben sie schon einmal Makros selber erstellt? (JN)
		-> Nein: warum nicht?
	- Habe Sie zus�tzlich noch Handmessmittel im Einsatz? (JN)
		-> Ja: welche?
	- Wie viele Mitarbeiter nutzen eine Messstation (Freitext)
	- Wird oft mit unterschiedlichen Einstellungen gemessen? (JN)
		-> Ja: W�rde Sie eine Profilverwaltung f�r die Mitarbeiter begr��en?
	- Ist die eingesetzte Software ihrer Meinung nach zum "einfach messen" geeignet? (JN)
		-> Nein
			- Warum nicht? Fehlende Funktionalit�t, Bedienung
		-> Ja
			- Was gef�llt Ihnen besonders gut?
			
	- Wie ist es notwendig, ein Messprotokoll auszudrucken? (JN)
		-> Ja: Wie oft; Darstellung und Umfang
		-> Nein: warum nicht? Arbeitsablauf erfordert es nicht; Probleme / Unzul�nglichkeiten damit
	
	- W�rden Sie ein alternatives Bedienkonzept zu Tastatur und Maus nutzen? (JN)
		-> Ja: Welches? Bsp: Touchscreen, Sprachsteuerung, andere
	
Scanware
	- 

U-Soft
	- 

MyCalypso
	- 

Umgebung
	- im Interview
	